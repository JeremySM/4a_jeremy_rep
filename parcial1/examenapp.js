const url = 'https://jsonplaceholder.typicode.com/todos';
const read = require('readline');

//esto es de node.js para poder leer, rl is used to read lines fmro user
const rl = read.createInterface({
    input: process.stdin,
    output: process.stdout
});

//obteniendo los datos del json
async function fetchData() {
        const response = await fetch(url);
        const todos = await response.json();
        return todos;
}

// display del menu
async function displayMenu() {
    const todos = await fetchData();

    
    // METODO PARA MOSTRAR IDS Y TITULOS para mostrar todo, lo que es true y lo que es false
    function mostrarTodos(todosList) {
        todosList.forEach(todo => {
            const status = todo.completed ? 'Resuelto' : 'No resuelto';
            console.log(`ID: ${todo.id}, Title: ${todo.title}, Estado: ${status}`);
        });
    }

    // METODO PARA MOSTRAR IDS Y USERID, para mostrar todo, lo que es true y lo que es false
    function mostrarTodos_id_userid(todosList) {
        todosList.forEach(todo => {
            const status = todo.completed ? 'Resuelto' : 'No resuelto';
            console.log(`ID: ${todo.id}, UserId: ${todo.userId}, Estado: ${status}`);
        });
    }

    // menu 
    console.log('|||||| Menú ||||||');
    console.log('1. Mostrar todos los pendientes (solo IDs)');
    console.log('2. Mostrar todos los pendientes (IDs y Titles)');
    console.log('3. Mostrar todos los pendientes SIN RESOLVER (ID y Title)');
    console.log('4. Mostrar todos los pendientes RESUELTOS (ID y Title)');
    console.log('5. Mostrar todos los pendientes (IDs y userID)');
    console.log('6. Mostrar todos los pendientes SIN RESOLVER (ID y userID)');
    console.log('7. Mostrar todos los pendientes RESUELTOS (ID y userID)'); // Nueva opción
    console.log('0. Salir');
    
    //NOTE FOR MYSELF: rl is read line
    rl.question('Seleccione una opción: ', (opcion) => {
        switch (opcion) {
            case '1':
                console.log('Lista de todos los pendientes (solo IDs):');
                todos.forEach(todo => console.log(todo.id));
                break;
            case '2':
                console.log('Lista de todos los pendientes (IDs y Titles):');
                mostrarTodos(todos);
                break;
            case '3':
                console.log('Lista de todos los pendientes sin resolver (ID y Title):');
                const unresolvedTodos = todos.filter(todo => !todo.completed);
                mostrarTodos(unresolvedTodos);
                break;
            case '4':
                console.log('Lista de todos los pendientes resueltos (ID y Title):');
                const resolvedTodos = todos.filter(todo => todo.completed);
                mostrarTodos(resolvedTodos);
                break;
            case '5':
                console.log('Lista de todos los pendientes (IDs y userID):');
                todos.forEach(todo => console.log(`ID: ${todo.id}, UserID: ${todo.userId}`));
                break;
            case '6':
                console.log('Lista de todos los pendientes sin resolver (ID y userID):');
                const unresolvedTodosUserID = todos.filter(todo => !todo.completed);
                mostrarTodos_id_userid(unresolvedTodosUserID);
                break;
            case '7':
                console.log('Lista de todos los pendientes resueltos (ID y userID):');
                const resolvedTodosUserID = todos.filter(todo => todo.completed);
                mostrarTodos_id_userid(resolvedTodosUserID);
                break;
            case '0':
                console.log('Adiós.');
                rl.close();
                break;
            default:
                console.log('Opción no válida. Por favor, elige una opción del 0 al 7.');
                break;
        }

        //shows the menu again en caso de no ser 0
        if (opcion !== '0') {
            displayMenu();
        }
    });
}

// Inicia el menú
displayMenu();
