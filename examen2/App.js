import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, ImageBackground, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const url = 'https://jsonplaceholder.typicode.com/todos';

const MenuScreen = ({ navigation }) => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const data = await response.json();
        setTodos(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);

  const displayTodos = (option) => {
    let filteredData;
    switch (option) {
      case 'Todos los pendientes':
        filteredData = todos;
        break;
      case 'Todos los pendientes (ID y Título)':
        filteredData = todos.map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case 'Pendientes sin completar (ID y Título)':
        filteredData = todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case 'Pendientes completados (ID y Título)':
        filteredData = todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case 'Todos los pendientes (ID y Usuario)':
        filteredData = todos.map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      case 'Pendientes sin completar (ID y Usuario)':
        filteredData = todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      case 'Pendientes completados (ID y Usuario)':
        filteredData = todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      default:
        filteredData = [];
    }
    navigation.navigate('TodoDetails', { todos: filteredData });
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView contentContainerStyle={styles.container}>
        <ImageBackground source={require('./assets/nfl_background.jpg')} style={styles.backgroundImage}>
          <Text style={styles.pageTitle}>Lista de Pendientes de la NFL</Text>
          {['Todos los pendientes',
            'Todos los pendientes (ID y Título)',
            'Pendientes sin completar (ID y Título)', 
            'Pendientes completados (ID y Título)', 
            'Todos los pendientes (ID y Usuario)', 
            'Pendientes sin completar (ID y Usuario)', 
            'Pendientes completados (ID y Usuario)'
          ].map(option => (
            <TouchableOpacity key={option} style={styles.optionButton} onPress={() => displayTodos(option)} >
              <Text style={styles.optionText}>{option}</Text>
            </TouchableOpacity>
          ))}
        </ImageBackground>
      </ScrollView>
    </SafeAreaView>
  );
};

const TodoDetailsScreen = ({ route }) => {
  const { todos } = route.params;

  return (
    <ScrollView style={styles.resultsContainer}>
      {todos.map((todo, index) => (
        <View key={todo.id.toString()} style={[styles.tableRow, index % 2 === 0 ? styles.redRow : styles.blueRow]}>
          <Text style={styles.tableCell}>{`ID: ${todo.id}`}</Text>
          {todo.title && <Text style={styles.tableCell}>{`Title: ${todo.title}`}</Text>}
          {todo.userId && <Text style={styles.tableCell}>{`User: ${todo.userId}`}</Text>}
        </View>
      ))}
    </ScrollView>
  );
};

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={MenuScreen} options={{ title: 'EXAMEN PARCIAL 2 : NFL' }} />
        <Stack.Screen name="TodoDetails" component={TodoDetailsScreen} options={{ title: 'Detalles del Todo' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'black', 
  },
  optionButton: {
    width: '70%',
    padding: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.5)', 
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: 'black', 
  },
  optionText: {
    color: 'black', 
    fontWeight: 'bold',
    textAlign: 'center',
  },
  resultsContainer: {
    padding: 10,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginHorizontal: 5,
  },
  tableCell: {
    flex: 1,
    fontSize: 16,
    color: 'black', 
  },
  redRow: {
    backgroundColor: '#FF5733',
  },
  blueRow: {
    backgroundColor: '#3498DB',
  },
});

export default App;
